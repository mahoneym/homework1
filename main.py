import numpy as np
import HW1_1

'''
Parameters
'''
phi =np.array([1.,0.95,-0.5])
sigma = 1
y_ = np.array([0.,0.])
epsilon_use=np.random.randn(50)


##############
# Part 1. a. #
##############



generate_y(phi,sigma,y_,50,epsilon_use)
##############
# Part 1. b. #
##############
generate_N_y(phi,sigma,y_,10,50)

for i in range(0,10):
    plt.plot(yn[i,:])

plt.show()
plt.close
##############
# Part 1. c. #
##############

generate_AC(phi,sigma)




##############
# Part 1. d. #
##############
x_ = np.hstack([y_,1.])

generate_x(x_,50,epsilon_use)
plt.plot(Z[0,:])
plt.plot(y)
plt.show()
'''
Notice that the two series lie "on top" of one another. That is,for the same 
epsilon vector the two functions return identical values.
'''
plt.close
##############
# Part 1. e. #
##############

generate_xhat(x_,50)
generate_N_y(phi,sigma,y_,500,50)
ybar=np.mean(yn,axis=0)
plt.plot(xhat[0,:])
plt.plot(ybar)
plt.show()

'''
The two series are quite close to one another. This is becasue the mean zero 
disturbance term is "shut down" in the function that generates xhat. Given 500
realizations of this process, at each time t the mean value of each realization
should be close to the realization if the shock never occured. That is, as the
number of trials increases, we should see the effects of each shock head to zero
in the mean. 
'''

plt.close