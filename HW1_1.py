import numpy as np

import matplotlib.pyplot as plt

global phi
phibar=np.array([1.,0.95,-0.5])

global y_
y_int=np.zeros(2)

global epsilon_use
epsilon_use=np.random.randn(51)

def generate_y(phi,sigma,y_,T,epsilon):
    global y
    y=np.zeros(T)
    y[0]=y_[0]
    y[1]=y_[1]
    for t in range(0,T-2):
        y[t+2]=phi[0]+phi[1]*y[t+1]+phi[2]*y[t]+sigma*epsilon[t+2]
    return y
    
    
    pass

def generate_N_y(phi,sigma,y_,N,T):
    global yn
    yn=np.zeros( (N,T), dtype=object )
    yn[:,0]=y_[0]
    yn[:,1]=y_[1]
    for n in range(0,N):
        for t in range(0,T-2):
            yn[(n,t+2)]=phi[0]+phi[1]*yn[(n,t+1)]+phi[2]*yn[(n,t)]+sigma*np.random.randn()
    return yn

    

    pass

def generate_AC(phi,sigma):
    global A
    global C
    A=np.array([[phi[1],phi[2],phi[0]],[1,0,0],[0,0,1]])
    C=np.array([sigma,0,0])
    return A,C
   
   
   
   
   
   
   
   
   

    pass

def generate_x(x_,T,epsilon):
   global Z
   Z=np.zeros((3,T))
   for i in range(0,3):
       Z[i,:]=x_[i]
   for t in range(2,T):
       Z[:,t]=A.dot(Z[:,t-1])+C*epsilon[t]
   return Z
   
   
   
   
   
   
   
   
   
   
  
   pass

def generate_xhat(xhat_,T):
    global xhat
    xhat=np.zeros((3,T))
    for i in range(0,3):
        xhat[i,:]=x_[i]
    for t in range(2,T):
        xhat[:,t]=A.dot(xhat[:,t-1])
    return xhat
    
    
    
    
    
    
    
    
   
    
    pass




